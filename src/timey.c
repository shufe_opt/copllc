#include <time.h>

double etime_(
float *usr)
/*float *sys)*/
{
    double ret_val;
    clock_t elaptime; 

    elaptime = clock();
    ret_val = (double) elaptime / (double) CLOCKS_PER_SEC ;
    return ret_val;

} /* etime_ */

