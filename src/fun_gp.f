c     function calls
c     LCCP
c     =======
c
c     obj <-- Function f(x)
c     c   <-- Gradient f(x)
c     H   <-- Hessian  f(x)

      function fun(n,c,ih,jh,hn,x)

      parameter (MaxColumns=20000, MaxRows=5000)
      common /LCCP/cc
      real*8  cc(MaxColumns)

      integer n, ih(1), jh(1)
      real*8  c(1), x(1), hn(1), fun

c     c is the gradient vector at decision vector x, and 
c     fun is the function value evaluated at x

      fun = 0.d0
      do j=1, n
	 fun = fun + x(j) * (dlog(x(j)) - dlog(cc(j)))
      end do
      fun = fun - (x(3)+x(4)) * dlog(x(3)+x(4))
c
      return
      end

      subroutine grad(n,c,ih,jh,hn,x)

      parameter (MaxColumns=20000, MaxRows=5000)
      common /LCCP/cc
      real*8  cc(MaxColumns)

      integer n, ih(1), jh(1)
      real*8  x(1), c(1), hn(1)

c     x is the decision vector and c is the gradient vector
c     evaluated at x

      do j = 1, n
	 c(j) = dlog(x(j)) + 1.d0 - dlog(cc(j))
      end do
      c(3) = c(3 ) - 1.d0 - dlog(x(3)+x(4))
      c(4) = c(4 ) - 1.d0 - dlog(x(3)+x(4))
c
      return
      end

      subroutine hessian(n,ih,jh,hn,x)

      parameter (MaxColumns=20000, MaxRows=5000)
      common /LCCP/cc
      real*8  cc(MaxColumns)

      integer n, ih(1), jh(1)
      real*8  x(1), hn(1)

c     x is the decision vector and hn is is the list of
c     diagonal and nonzero entries in the up-half of the
c     Hessian evaluated at x, with the same order as the list
c     in the data file ``test.dat''.

      do j = 1, 2
	 hn(j) = 1.d0/x(j)
      end do
      hn(3) = 1.d0/x(3)-1.d0/(x(3)+x(4))
      hn(4) = -1.d0/(x(3)+x(4))
      hn(5) = 1.d0/x(4)-1.d0/(x(3)+x(4)) 
c
      return
      end

